<?php
/**
 * @file
 * Template for 'Custom Items'... which are fieldable panels panes.
 * They have the same layout as teasers and featured view modes.
 * The layout is oe-basic which is defined in the module: Ethical Theme
 * That layout is copied - with some small additions - in this file.
 * In particular the view mode - featured or teaser - is checked here in order to determine what to display.
 */

  $view_mode = check_plain($content['field_custom_view_mode']['#items'][0]['value']);
  $class = '';
  $title = '';
  $options = array('html' => true);

  if(empty($content['sidebar'])){
    if (!empty($class)) {
      $class .= ' no-sidebar';
    }
    else {
      $class = 'no-sidebar';
    }
  }

  if(empty($content['header'])){
    if (!empty($class)) {
      $class .= ' no-header';
    }
    else {
      $class = 'no-header';
    }
  }

  if(empty($content['field_custom_text'])){
    if (!empty($class)) {
      $class .= ' no-text';
    }
    else {
      $class = 'no-text';
    }
  }

  if($class != ''){
    $class = 'class=' . $class;
  }

  if(!empty($content['field_custom_link_title'])){
    $field_custom_link_title = $content['field_custom_link_title'][0]['#markup'];
  }
  else{
    $field_custom_link_title = t('Read more');
  }


  if(!empty($content['field_custom_title'])){
    if(!empty($content['field_custom_link'])){
      $title = l($content['field_custom_title'][0]['#markup'], $content['field_custom_link']['#items'][0]['display_url'], $options);
    }
    else{
      $title = $content['field_custom_title'][0]['#markup'];
    }
  }

  if($title != ''){
    $title = '<h3>' . $title . '</h3>';
  }


?>
<div class="oe-<?php print $view_mode; ?>">
  <div class="panel-display oe-basic clearfix <?php print $class ?>">
  <?php

  if($view_mode == 'teaser' && !empty($title)){ ?>

  <div class="header">
    <?php print $title; ?>
  </div>

  <?php }

  if(!empty($content['field_custom_image'])){
  ?>
    <div class="sidebar">
      <div class="field-featured-image">
        <?php
          if(!empty($content['field_custom_link'])){
            print l(render($content['field_custom_image']), $content['field_custom_link']['#items'][0]['display_url'], $options);
          }
          else{
            print render($content['field_custom_image']);
          }
        ?>
      </div>
    </div>
  <?php
  }
  ?>
    <div class="contentmain">
    <?php
    if($view_mode == 'featured' && !empty($title)){
      print $title;
    }
    ?>
    <?php
    if(!empty($content['field_custom_text'])){
    ?>
      <div class="field-body">
        <?php
          print render($content['field_custom_text']);
        ?>
      </div>
    <?php
    }
    ?>
    <?php
    if(!empty($content['field_custom_link'])){
    ?>
      <ul class="links inline">
        <li class="first last">
          <?php print l($field_custom_link_title, $content['field_custom_link']['#items'][0]['display_url'], $options);?>
        </li>
      </ul>
    <?php
    }
    ?>
    </div>
  </div><!-- /.oe-basic -->
</div>
