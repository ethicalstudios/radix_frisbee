<!-- STRIPED SECTION CONTENT WITH QUOTATION -->

<div class="panel-display stripe-content-with-quote clearfix">
  <?php if(!empty($content['group_quote']['field_oe_quote_content'])){ ?>
  <div class="stripe-quotation col-md-4">
    <blockquote>
      <?php print render($content['group_quote']['field_oe_quote_content']); ?>
      <?php if(!empty($content['group_quote']['field_oe_quote_name'])){ ?>
      <div class="quote-author">
        <?php if(!empty($content['group_quote']['field_oe_quote_image'])){ ?>
        <?php print render($content['group_quote']['field_oe_quote_image']); ?>
        <?php } ?>
        <p><strong>&mdash; <?php print render($content['group_quote']['field_oe_quote_name']); ?></strong></p>
        <?php if(!empty($content['group_quote']['field_oe_quote_role'])){ ?>
        <p><?php print render($content['group_quote']['field_oe_quote_role']); ?>.</p>
        <?php } ?>
      </div>
      <?php } ?>
    </blockquote>
  </div>
  <?php } ?>
  <?php if(!empty($content['field_oe_section_content'])){ ?>
  <div class="stripe-content col-md-8">
    <?php print render($content['field_oe_section_content']); ?>
    <?php if(!empty($content['field_cta_link'])){ ?>
      <?php print render($content['field_cta_link']); ?>
    <?php } ?>
  </div>
  <?php } ?>

</div>    <!-- end .stripe-content-with-quote -->
