<?php

/**
 * @file
 * Theme template for a list of tweets.
 *
 * Available variables in the theme include:
 *
 * 1) An array of $tweets, where each tweet object has:
 *   $tweet->id
 *   $tweet->username
 *   $tweet->userphoto
 *   $tweet->text
 *   $tweet->timestamp
 *
 * 2) $twitkey string containing initial keyword.
 *
 * 3) $title
 *
 */
?>

<?php if (is_array($tweets)): ?>

  <?php $tweet_count = count($tweets); ?>

  <ul id="twitter">
  <?php foreach ($tweets as $tweet_key => $tweet): ?>
    <li rel="<?php print $tweet->time_ago; ?>">
      <div class="tweet"><p><a target="_blank" title="Opens in a new window" href="http://www.twitter.com/<?php print $tweet->screenname; ?>">
        <strong>@<?php print $tweet->screenname; ?></strong></a> <?php print twitter_pull_add_links($tweet->text); ?></p>
          <ul>
            <li class="createdTime"><?php print $tweet->time_ago; ?></li>
            <li><a target="_blank" title="Opens in a new window" href="http://twitter.com/intent/tweet?in_reply_to=<?php print $tweet->id; ?>">reply</a></li>
            <!--<li><a target="_blank" title="Opens in a new window" href="http://twitter.com/intent/retweet?tweet_id=<?php print $tweet->id; ?>">retweet</a></li>
            <li><a target="_blank" title="Opens in a new window" href="http://twitter.com/intent/favorite?tweet_id=<?php print $tweet->id; ?>">favorite</a></li>-->
          </ul>
      </div>
    </li>
  <?php endforeach; ?>
  </ul>
<?php endif; ?>

