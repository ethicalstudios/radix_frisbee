<?php
/**
 * @file field--fences-tags_commas.tpl.php
 * End each term value with a comma.
 *
 */
?>
<div class="tags <?php print $label; ?>">
<?php if ($element['#label_display'] == 'inline'): ?>
  <p>
    <?php print $label; ?>:
<?php elseif ($element['#label_display'] == 'above'): ?>
  <h3 class="field-label"<?php print $title_attributes; ?>>
    <?php print $label; ?>
  </h3>
  <p>
<?php endif; ?>
  <?php
    $num_items = count($items);
    $i = 0;
  ?>
  <?php foreach ($items as $delta => $item): ?>
      <?php
        $i++;
        print render($item);

        if($i != $num_items){
          print ', ';
        }
      ?>
  <?php endforeach; ?>

</p></div>
