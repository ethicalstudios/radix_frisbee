<?php
// Themes files of type document
// Done this way because panelizer does not yet work for file entities and because this way seems to work with translations


if(!empty($content['file'])){
  print render($content['file']);
}
