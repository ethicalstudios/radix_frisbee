<?php
// Themes files of type document
// Done this way because panelizer does not yet work for file entities

$class = '';

if(empty($content['field_featured_image'])){
  $class = ' no-sidebar';
}
?>


<div class="panel-display oe-basic clearfix no-header<?php print $class ?>">
  <?php
  if(!empty($content['field_featured_image'])){
  ?>
    <div class="sidebar">
      <?php print render($content['field_featured_image']); ?>
    </div>
  <?php
  }
  ?>
    <div class="contentmain">
      <?php
      if(!empty($content['field_file_description'])){
        print render($content['field_file_description']);
      }
      if(!empty($content['file'])){
        print render($content['file']);
      }
      ?>
    </div>
</div><!-- /.oe-basic -->
