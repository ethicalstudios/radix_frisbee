<?php
// Themes files of type audio
// Done this way because panelizer does not yet work for file entities
?>
<div class="oe-featured">
  <div class="panel-display oe-basic clearfix no-header">

      <div class="sidebar">
        <?php print render($content['file']); ?>
      </div>

      <div class="contentmain">
        <?php
        if(!empty($content['filename_field'])){
          print render($content['filename_field']);
        }
        if(!empty($content['field_file_description'])){
          print render($content['field_file_description']);
        }
        ?>
      </div>
  </div><!-- /.oe-basic -->
</div>
