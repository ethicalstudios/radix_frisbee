<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 */
?>
<header id="header" class="header" role="banner">
  <div class="container">
    <nav class="navbar" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div id="navbar-header" class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
          <span class="sr-only"><?php print t('Toggle Menu'); ?></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <?php if ($site_name || $logo): ?>
          <a href="<?php print $front_page; ?>" class="navbar-brand" rel="home" title="<?php print t('Home'); ?>">
            <?php if ($logo): ?>
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" />
            <?php endif; ?>
            <?php if ($site_name): ?>
              <span class="site-name sr-only"><?php print $site_name; ?></span>
            <?php endif; ?>
          </a>
        <?php endif; ?>
      </div> <!-- /.navbar-header -->

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="navbar-collapse">
        <?php if ($main_menu): ?>
          <ul id="main-menu" class="menu nav navbar-nav">
            <?php print render($main_menu); ?>
          </ul>
        <?php endif; ?>
        <div id="header-items" class="header-row">
          <?php if ($secondary_menu): ?>
            <ul id="secondary-menu" class="menu nav navbar-nav secondary-menu">
              <?php print render($secondary_menu); ?>
            </ul> <!-- /#secondary-menu -->
          <?php endif; ?>
          <?php if ($search_form): ?>
            <div role="search">
              <?php print $search_form; ?>
            </div>
          <?php endif; ?>
        </div>
        <?php if(module_exists('ethical_translation')): ?>
            <h4 class="sr-only">Language</h4>
            <?php
            $block = module_invoke('locale', 'block_view', 'language');
            print $block['content'];
            ?>
        <?php endif; ?>
      </div><!-- /.navbar-collapse -->
    </nav><!-- /.navbar -->
  </div> <!-- /.container -->
</header>

<div id="main-wrapper">
  <main id="main" class="main <?php if (!$is_panel) { print 'container'; } ?>" role="main">
    <div class="container">
      <?php if ($breadcrumb): ?>
        <div id="breadcrumb" class="visible-desktop">
          <?php print $breadcrumb; ?>
        </div>
      <?php endif; ?>
      <?php if ($messages): ?>
        <div id="messages">
          <?php print $messages; ?>
        </div>
      <?php endif; ?>
      <div id="page-header">
        <?php if ($title): ?>
          <div class="page-header">
            <h1 class="title"><?php print $title; ?></h1>
          </div>
        <?php endif; ?>
        <?php if ($tabs): ?>
          <div class="tabs">
            <?php print render($tabs); ?>
          </div>
        <?php endif; ?>
        <?php if ($action_links): ?>
          <ul class="action-links">
            <?php print render($action_links); ?>
          </ul>
        <?php endif; ?>
      </div>
    </div>
    <div id="content" class="container<?php if(!empty($node) && $node->type == 'oe_story'){ print '-fluid';};  ?>">
      <?php print render($page['content']); ?>
    </div>
  </main> <!-- /#main -->
</div> <!-- /#main-wrapper -->

<footer id="footer" class="footer" role="contentinfo">
  <div class="container">
    <div class="row">
      <div class="pull-left col-md-4 clearfix">
        <?php print render($page['footer_firstcolumn']); ?>
      </div>
      <div class="pull-left col-md-4 clearfix">
        <?php print render($page['footer_secondcolumn']); ?>
      </div>
      <div class="pull-left col-md-4 last clearfix">
        <?php print render($page['footer_thirdcolumn']); ?>
      </div>
    </div>
    <div id="page-end">
      <div class="container">
        <?php print render($page['footer_fourthcolumn']); ?>
      </div>
    </div>
    <small class="link-to-top pull-right"><a href="#"><?php print t('Back to Top'); ?></a></small>
  </div>
</footer>
